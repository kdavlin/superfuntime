﻿namespace SuperFuntime
{
    using System.Windows;
    using System.Reflection;
    using System.Drawing;

    using NetSparkle;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Sparkle _sparkle;

        public MainWindow()
        {
            InitializeComponent();

            VersionLabel.Content = Assembly.GetExecutingAssembly().GetName().Version;

            _sparkle = new Sparkle("http://update.davlinski.com/superfuntime/versioninfo.xml", SystemIcons.Application);
            _sparkle.StartLoop(true, true);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            _sparkle.StopLoop();
        }
    }
}
